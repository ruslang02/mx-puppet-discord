/* tslint:disable: no-any */
/*
Copyright 2022 mx-puppet-discord
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * Return a function which calls oldFunc then newFunc, adding what
 * oldFunc returned as the first argument of newFunc
 * @param oldFunc Function called before the new function
 * @param newFunc Function called after the old function
 */
export function patchAfter(
	oldFunc: (...args: any[]) => any,
	newFunc: (this: any, returnValue: any, ...args: any[]) => any,
): (...args: any[]) => any {
	function patch(this: any, ...args: any[]) {
		const ret = oldFunc.apply(this, args);

		return newFunc.apply(this, [ret, ...args]);
	}

	return patch;
}

/**
 * Return a function which calls newFunc, and if it didn't
 * return anything, calls oldFunc
 * @param oldFunc Function called after the new function
 * @param newFunc Function called before the old function
 */
export function patchBefore(
	oldFunc: (...args: any[]) => any,
	newFunc: (this: any, ...args: any[]) => any,
): (...args: any[]) => any {
	function patch(this: any, ...args: any[]) {
	const ret = newFunc.apply(this, args);

	return typeof ret === "undefined" ? oldFunc.apply(this, args) : ret;
	}

	return patch;
}

/**
 * Monkey patch a class into another. Optional patch's members are discarded
 * @param prototypeToPatch Prototype of the class to patch
 * @param PatchClass Class used as a patch
 */
export function patchClass(prototypeToPatch: any, PatchClass: any) {
	const properties = {
		...Object.getOwnPropertyDescriptors(new PatchClass()),
		...Object.getOwnPropertyDescriptors(PatchClass.prototype),
	};

	Object.defineProperties(prototypeToPatch, properties);
}
